<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Estate Appointment System | API Documentation</title>
    <meta name="description" content="Estate Appointment System | API Documentation">
    <meta name="keywords" content='Estate Appointment System | API Documentation'>

    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="lang" content="en" />
    <meta name="author" content="Hasan Bekir DOĞAN" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="robots" content="noindex, follow" />
    <meta name="csrf-token" content="{{ csrf_token() }}">



    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/layout.css')}}">

</head>
<body class="antialiased">

<div style="margin:5% 15%">


    <h1 style="text-align: center">Documentation</h1>

    <hr>

    <h3>Introduction</h3>
    <p>
        Welcome to the Estate Appointment API! This documentation should help you familiarise yourself with the
        resources available and how to consume them with HTTP requests. If you're after a native helper library
        then I suggest you scroll down and check out what's available. Read through the getting started section
        before you dive in. Most of your problems should be solved just by reading through it.
    </p>

    <hr>

    <h3>Base URL</h3>
    <p>The Base URL is the root URL for all of the API, if you ever make a request to api and you get back a 404 NOT FOUND response then check the Base URL first.</p>
    <p>The Base URL for api is:</p>
    <pre><code>https://estate-appointment-system.herokuapp.com/api</code></pre>
    <p>The documentation below assumes you are prepending the Base URL to the endpoints in order to make requests.</p>

    <hr>

    <h3>Bearer Token</h3>
    <p>When you login to the system, you will see a new API token listed called Access Token. This token is
        expected to be sent along as a Authorization header. A simple cURL example looks like the following:</p>
    <pre><code>curl --request GET \
    --url 'https://estate-appointment-system.herokuapp.com/api/appointment/2' \
    --header 'Authorization: Bearer << access_token >>' \
    --header 'Content-Type: application/json;charset=utf-8'</code></pre>

    <br>
    <br>
    <br>
    <br>
    <br>

    <h3>User Authentication</h3>
    <p>User authentication is controlled with a access token query parameter. You have to register/login/refresh to the system to generate a access token.</p>

    <hr>

    <h3>Register</h3>
    <p><b class="blue">POST</b> /register</p>
    <p>You can get access token to authenticate.</p>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Request Body (application/json) <i>- example</i></h5>
    <pre><code>{
    "name": "bekir",
    "email": "bekir@gmail.com",
    "password": "bekir123",
    "password_confirmation": "bekir123"
}</code></pre>
    <table>
        <tr>
            <td>name:</td>
            <td>required</td>
        </tr>
        <tr>
            <td>email:</td>
            <td>required</td>
        </tr>
        <tr>
            <td>password:</td>
            <td>required</td>
        </tr>
        <tr>
            <td>password_confirmation:</td>
            <td>required</td>
        </tr>
    </table>
    <br>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "message": "User successfully registered",
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9lc3RhdGUtYXBwb2ludG1lbnQtc3lzdGVtLmhlcm9rdWFwcC5jb21cL2FwaVwvcmVnaXN0ZXIiLCJpYXQiOjE2Mzg5OTU0NTMsImV4cCI6MTYzODk5OTA1MywibmJmIjoxNjM4OTk1NDUzLCJqdGkiOiJIZVdEcHExSFdqOVlTOVV0Iiwic3ViIjoyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.e9cX334u94ydno84w3BrRyU0m0z9-hV_1kWpsP8_M8g",
    "token_type": "bearer",
    "expires_in": 3600,
    "user": {
        "name": "Bekir",
        "email": "bekir@gmail.com",
        "updated_at": "2021-12-08T20:30:53.000000Z",
        "created_at": "2021-12-08T20:30:53.000000Z",
        "id": 2
    }
}</code></pre>

    <hr>

    <h3>Login</h3>
    <p><b class="blue">POST</b> /login</p>
    <p>You can get access token to authenticate.</p>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Request Body (application/json) <i>- example</i></h5>
    <pre><code>{
    "email": "bekir@gmail.com",
    "password": "bekir123"
}</code></pre>
    <table>
        <tr>
            <td>email:</td>
            <td>required</td>
        </tr>
        <tr>
            <td>password:</td>
            <td>required</td>
        </tr>
    </table>
    <br>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9lc3RhdGUtYXBwb2ludG1lbnQtc3lzdGVtLmhlcm9rdWFwcC5jb21cL2FwaVwvbG9naW4iLCJpYXQiOjE2Mzg5OTU1NDgsImV4cCI6MTYzODk5OTE0OCwibmJmIjoxNjM4OTk1NTQ4LCJqdGkiOiJScklPc2lBalVYSGFpcGxMIiwic3ViIjoyLCJwcnYiOiIyM2JkNWM4OTQ5ZjYwMGFkYjM5ZTcwMWM0MDA4NzJkYjdhNTk3NmY3In0.wVhzH_2tRvSCF2e_nYzj2XzTZCn9xOiHxvak6ftxbtc",
    "token_type": "bearer",
    "expires_in": 3600,
    "user": {
        "id": 2,
        "name": "Bekir",
        "email": "bekir@gmail.com",
        "email_verified_at": null,
        "created_at": "2021-12-08T20:30:53.000000Z",
        "updated_at": "2021-12-08T20:30:53.000000Z"
    }
}</code></pre>

    <hr>

    <h3>Refresh API Token</h3>
    <p><b class="blue">POST</b> /refresh</p>
    <p>After you login to the system, you can generate a new access token.</p>
    <h5>Authentication</h5>
    <pre><code>Bearer Token</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Query String</h5>
    <pre><code>Bearer Token: access token</code></pre>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9lc3RhdGUtYXBwb2ludG1lbnQtc3lzdGVtLmhlcm9rdWFwcC5jb21cL2FwaVwvcmVmcmVzaCIsImlhdCI6MTYzODk5NTU0OCwiZXhwIjoxNjM4OTk5NjMyLCJuYmYiOjE2Mzg5OTYwMzIsImp0aSI6IjZDTWxBTEhkeUIzTmRPVm0iLCJzdWIiOjIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.jXVGN5DSqqe90Lt2gmXjs4k4iN3zPixdqx_KTWNBEuQ",
    "token_type": "bearer",
    "expires_in": 3600,
    "user": {
        "id": 2,
        "name": "Bekir",
        "email": "bekir@gmail.com",
        "email_verified_at": null,
        "created_at": "2021-12-08T20:30:53.000000Z",
        "updated_at": "2021-12-08T20:30:53.000000Z"
    }
}</code></pre>

    <hr>

    <h3>User Profile</h3>
    <p><b class="green">GET</b> /user-profile</p>
    <p>After you login to the system, you can show your user information.</p>
    <h5>Authentication</h5>
    <pre><code>Bearer Token</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Query String</h5>
    <pre><code>Bearer Token: access token</code></pre>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "id": 2,
    "name": "Bekir",
    "email": "bekir@gmail.com",
    "email_verified_at": null,
    "created_at": "2021-12-08T20:30:53.000000Z",
    "updated_at": "2021-12-08T20:30:53.000000Z"
}</code></pre>

    <hr>

    <h3>Logout</h3>
    <p><b class="blue">POST</b> /logout</p>
    <p>After you login to the system, you can logout from the system.</p>
    <h5>Authentication</h5>
    <pre><code>Bearer Token</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Query String</h5>
    <pre><code>Bearer Token: access token</code></pre>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "message": "User successfully signed out"
}</code></pre>

    <br>
    <br>
    <br>
    <br>
    <br>

    <h3>Create an Appointment</h3>
    <p><b class="blue">POST</b> /appointments</p>
    <p>You can create a new appointment.</p>
    <h5>Authentication</h5>
    <pre><code>Bearer Token</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Query String</h5>
    <pre><code>Bearer Token: access token</code></pre>
    <h5>Request Body (application/json) <i>- example</i></h5>
    <pre><code>{
    "name": "Hasan",
    "surname": "Doğan",
    "email": "hasan@gmail.com",
    "phone": "12345678",
    "address": "M32 0JG"
}</code></pre>
    <table style="min-width: 700px">
        <tr>
            <td>name:</td>
            <td colspan="2">required</td>
        </tr>
        <tr>
            <td>surname:</td>
            <td colspan="2">required</td>
        </tr>
        <tr>
            <td>email:</td>
            <td colspan="2">required</td>
        </tr>
        <tr>
            <td>phone:</td>
            <td colspan="2">required</td>
        </tr>
        <tr>
            <td>address:</td>
            <td>required</td>
            <td><i>have to be entered as a postcode</i></td>
        </tr>
    </table>
    <br>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "message": "Appointment successfully updated",
    "appointment": {
        "id": 2,
        "user_id": 2,
        "user_name": "Bekir",
        "distance": "359 km",
        "leave_time": "2021-12-09T04:11:17.000000Z",
        "arrival_time": "2021-12-09T12:54:17.000000Z",
        "customer_name": "Hasan",
        "customer_surname": "Doğan",
        "customer_email": "hasan@gmail.com",
        "customer_phone": "12345678",
        "customer_postcode": "M32 0JG",
        "customer_explicit_address": "Nansen St, Stretford, Manchester M32 0JG, UK"
    }
}</code></pre>

    <hr>

    <h3>Get Appointments</h3>
    <p><b class="green">GET</b> /appointments</p>
    <p>You can get appointments that you have.</p>
    <h5>Authentication</h5>
    <pre><code>Bearer Token</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Query String</h5>
    <pre><code>Bearer Token: access token</code></pre>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>[
    {
        "id": 2,
        "user_id": 2,
        "user_name": "Bekir",
        "distance": "359 km",
        "leave_time": "2021-12-09 04:11:17",
        "arrival_time": "2021-12-09 12:54:17",
        "created_at": "2021-12-08T20:52:17.000000Z",
        "customer_name": "Hasan",
        "customer_surname": "Doğan",
        "customer_email": "hasan@gmail.com",
        "customer_phone": "12345678",
        "customer_postcode": "M32 0JG",
        "customer_explicit_address": "Nansen St, Stretford, Manchester M32 0JG, UK"
    }
]</code></pre>

    <hr>

    <h3>Get appointments by filter using date</h3>
    <p><b class="green">GET</b> /appointments</p>
    <p>You can get appointments that you have by filter according to date. This date process filters on the date that the appointments were created.</p>
    <h5>Authentication</h5>
    <pre><code>Bearer Token</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Query String</h5>
    <pre><code>Bearer Token: access token
Date: string</code></pre>
    <p>Example 1 URL api is:</p>
    <pre><code>https://estate-appointment-system.herokuapp.com/api/appointments?date=2021/12/07 15:51:24 - 2021/12/07 19:11:50</code></pre>
    <p>Example 2 URL api is:</p>
    <pre><code>https://estate-appointment-system.herokuapp.com/api/appointments?date=2021/12/07 - 2021/12/07</code></pre>
    <p style="color:red">Note: Date and time format have to be written as shown above.</p>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>[
    {
        "id": 2,
        "employee_name": "Bekir",
        "distance": "359 km",
        "leave_time": "2021-12-09 04:11:17",
        "arrival_time": "2021-12-09 12:54:17",
        "created_at": "2021-12-08T21:26:32.000000Z",
        "customer_name": "Hasan",
        "customer_surname": "Doğan",
        "customer_email": "hasan@gmail.com",
        "customer_phone": "12345678",
        "customer_postcode": "M32 0JG",
        "customer_explicit_address": "Nansen St, Stretford, Manchester M32 0JG, UK"
    }
]</code></pre>

    <hr>

    <h3>Get an Appointment</h3>
    <p><b class="green">GET</b> /appointments/{appointment_id}</p>
    <p>You can get an appointment that you have.</p>
    <h5>Authentication</h5>
    <pre><code>Bearer Token</code></pre>
    <h5>Path Parameters</h5>
    <pre><code>appointment_id: integer</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Query String</h5>
    <pre><code>Bearer Token: access token</code></pre>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>[
    {
        "id": 2,
        "user_id": 2,
        "user_name": "Bekir",
        "distance": "359 km",
        "leave_time": "2021-12-09 04:11:17",
        "arrival_time": "2021-12-09 12:54:17",
        "created_at": "2021-12-08T20:52:17.000000Z",
        "customer_name": "Hasan",
        "customer_surname": "Doğan",
        "customer_email": "hasan@gmail.com",
        "customer_phone": "12345678",
        "customer_postcode": "M32 0JG",
        "customer_explicit_address": "Nansen St, Stretford, Manchester M32 0JG, UK"
    }
]</code></pre>

    <hr>

    <h3>Update an Appointment</h3>
    <p><b class="orange">PUT</b> /appointments/{appointment_id}</p>
    <p>You can update an appointment that you have.</p>
    <h5>Authentication</h5>
    <pre><code>Bearer Token</code></pre>
    <h5>Path Parameters</h5>
    <pre><code>appointment_id: integer</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Query String</h5>
    <pre><code>Bearer Token: access token</code></pre>
    <h5>Request Body (application/json) <i>- example</i></h5>
    <pre><code>{
    "name": "Ahmet",
    "surname": "Çakmak",
    "email": "ahmet@gmail.com",
    "phone": "7894165",
    "address": "OX49 5NU"
}</code></pre>
    <table style="min-width: 700px">
        <tr>
            <td>name:</td>
            <td colspan="2">optional</td>
        </tr>
        <tr>
            <td>surname:</td>
            <td colspan="2">optional</td>
        </tr>
        <tr>
            <td>email:</td>
            <td colspan="2">optional</td>
        </tr>
        <tr>
            <td>phone:</td>
            <td colspan="2">optional</td>
        </tr>
        <tr>
            <td>address:</td>
            <td>optional</td>
            <td><i>have to be entered as a postcode</i></td>
        </tr>
    </table>
    <br>
    <h5>Response (application/json) <i>- example</i></h5>
    <pre><code>{
    "message": "Appointment successfully updated",
    "appointment": [
        {
            "id": 2,
            "user_id": 2,
            "user_name": "Bekir",
            "distance": "142 km",
            "leave_time": "2021-12-09 04:11:17",
            "arrival_time": "2021-12-09 08:28:17",
            "created_at": "2021-12-08T20:52:17.000000Z",
            "customer_name": "Ahmet",
            "customer_surname": "Çakmak",
            "customer_email": "ahmet@gmail.com",
            "customer_phone": "7894165",
            "customer_postcode": "OX49 5NU",
            "customer_explicit_address": "Brightwell Baldwin, Watlington OX49 5NU, UK"
        }
    ]
}</code></pre>

    <hr>

    <h3>Delete an Appointment</h3>
    <p><b class="red">DELETE</b> /appointments/{appointment_id}</p>
    <p>You can update an appointment that you have.</p>
    <h5>Authentication</h5>
    <pre><code>Bearer Token</code></pre>
    <h5>Path Parameters</h5>
    <pre><code>appointment_id: integer</code></pre>
    <h5>Headers</h5>
    <pre><code>Content-Type: application/json</code></pre>
    <h5>Query String</h5>
    <pre><code>Bearer Token: access token</code></pre>
    <h5>Response (application/json) <i>- example</i></h5>
    <table>
        <tr>
            <td>No content</td>
            <td>Status 204</td>
        </tr>
    </table>



</div>


    <script type="text/javascript" src="{{asset('assets/js/bootstrap.min.js')}}"></script>

</body>
</html>
