
**Project Name:** Estate Appointment System

**Project Description:** A system where estate employee can register to this API system and create appointments.

##

**Users can do these functions:**

- Account
    - register, login, logout
    - get api token
    - show user profile information
- Appointment
    - list appointments
    - list appointments and filter according to date
    - show an appointment
    - create appointments
    - update appointments
    - delete appointments

##

#### Install & Configure JWT Authentication Package
Execute the following command to install tymondesigns/jwt-auth, It is a third-party JWT package and allows user authentication using JSON Web Token in Laravel & Lumen securely.

    composer require tymon/jwt-auth

In the next step, we have to publish the package’s configuration, following command copy JWT Auth files from vendor folder to config/jwt.php file.

    php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"

For handling the token encryption, generate a secret key by executing the following command.

    php artisan jwt:secret

We have successfully generated the JWT Secret key, and you can check this key inside the .env file.

    JWT_SECRET=secret_jwt_string_key

##

### Documentation

API documentation is located at [https://estate-appointment-system.herokuapp.com](https://estate-appointment-system.herokuapp.com)

##

**Technologies that used in the project.**

- Object Oriented Programming
- PHP
- Laravel Framework
- Laravel Repository Pattern
- JSON
- SQL
- PostgreSQL
- REST API
