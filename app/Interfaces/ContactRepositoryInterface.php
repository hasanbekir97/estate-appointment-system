<?php

namespace App\Interfaces;

interface ContactRepositoryInterface
{
    public function createContact(array $contactDetails);
    public function updateContact(array $contactDetails, $appointmentId);
    public function deleteContact($appointmentId);
}
