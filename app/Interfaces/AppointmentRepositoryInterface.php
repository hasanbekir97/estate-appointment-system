<?php

namespace App\Interfaces;

interface AppointmentRepositoryInterface
{
    public function getAllAppointments();
    public function getAppointment($appointmentId);
    public function getFilteredAppointments($filter);
    public function createAppointment(array $appointmentDetails);
    public function updateAppointment(array $appointmentDetails, $appointmentId);
    public function deleteAppointment($appointmentId);
}
