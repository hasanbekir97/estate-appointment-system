<?php

namespace App\Interfaces;

interface UserRepositoryInterface
{
    public function loginUser(array $userDetails);
    public function createUser(array $userDetails);
    public function getUser();
    public function refreshUserToken();
    public function logoutUser();
}
