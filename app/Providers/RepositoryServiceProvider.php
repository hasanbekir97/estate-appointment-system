<?php

namespace App\Providers;

use App\Interfaces\AppointmentRepositoryInterface;
use App\Interfaces\ContactRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Repositories\AppointmentRepository;
use App\Repositories\ContactRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AppointmentRepositoryInterface::class, AppointmentRepository::class);
        $this->app->bind(ContactRepositoryInterface::class, ContactRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
