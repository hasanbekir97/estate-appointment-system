<?php

namespace App\Repositories;

use App\Interfaces\ContactRepositoryInterface;
use App\Models\Contact;
use Illuminate\Support\Facades\Auth;

class ContactRepository implements ContactRepositoryInterface
{
    public function createContact(array $contactDetails){
        return Contact::create($contactDetails);
    }
    public function updateContact(array $contactDetails, $appointmentId){
        $contact = Contact::findOrFail($appointmentId);
        $contact->update($contactDetails);
    }
    public function deleteContact($appointmentId){
        return Contact::where('user_id', auth()->user()->id)
            ->where('appointment_id', $appointmentId)
            ->delete();
    }
}
