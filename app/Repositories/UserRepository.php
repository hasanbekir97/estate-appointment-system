<?php

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class UserRepository implements UserRepositoryInterface
{
    public function loginUser(array $userDetails){
        return auth()->attempt($userDetails);
    }
    public function createUser(array $userDetails){
        return User::create($userDetails);
    }
    public function getUser(){
        return auth()->user();
    }
    public function refreshUserToken(){
        return auth()->refresh();
    }
    public function logoutUser(){
        return auth()->logout();
    }
}
