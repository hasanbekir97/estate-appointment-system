<?php

namespace App\Repositories;

use App\Interfaces\AppointmentRepositoryInterface;
use App\Models\Appointment;
use Illuminate\Support\Facades\Auth;

class AppointmentRepository implements AppointmentRepositoryInterface
{
    public function getAllAppointments(){
        return Appointment::selectRaw('appointments.id, users.id as user_id, users.name as user_name, appointments.distance,
                                       appointments.leave_time, appointments.arrival_time, appointments.created_at,
                                       contacts.name as customer_name, contacts.surname as customer_surname,
                                       contacts.email as customer_email, contacts.phone as customer_phone,
                                       contacts.postcode as customer_postcode, contacts.explicit_address as customer_explicit_address')
            ->where('appointments.user_id', auth()->user()->id)
            ->join('contacts', function ($join) {
                $join->on('appointments.id', '=', 'contacts.appointment_id');
            })
            ->join('users', function ($join) {
                $join->on('appointments.user_id', '=', 'users.id');
            })
            ->get();
    }
    public function getAppointment($appointmentId){
        return Appointment::selectRaw('appointments.id, users.id as user_id, users.name as user_name, appointments.distance,
                                       appointments.leave_time, appointments.arrival_time, appointments.created_at,
                                       contacts.name as customer_name, contacts.surname as customer_surname,
                                       contacts.email as customer_email, contacts.phone as customer_phone,
                                       contacts.postcode as customer_postcode, contacts.explicit_address as customer_explicit_address')
            ->where('appointments.user_id', auth()->user()->id)
            ->where('appointments.id', $appointmentId)
            ->join('contacts', function ($join) {
                $join->on('appointments.id', '=', 'contacts.appointment_id');
            })
            ->join('users', function ($join) {
                $join->on('appointments.user_id', '=', 'users.id');
            })
            ->get();
    }
    public function getFilteredAppointments($filter){
        $dates = explode("-", $filter);
        $beginDate = $dates[0];
        $endDate = $dates[1];

        return Appointment::selectRaw('appointments.id, users.name as employee_name, appointments.distance,
                                       appointments.leave_time, appointments.arrival_time, appointments.created_at,
                                       contacts.name as customer_name, contacts.surname as customer_surname,
                                       contacts.email as customer_email, contacts.phone as customer_phone,
                                       contacts.postcode as customer_postcode, contacts.explicit_address as customer_explicit_address')
            ->where('appointments.user_id', auth()->user()->id)
            ->where('appointments.created_at', '>=', $beginDate)
            ->where('appointments.created_at', '<=', $endDate)
            ->join('contacts', function ($join) {
                $join->on('appointments.id', '=', 'contacts.appointment_id');
            })
            ->join('users', function ($join) {
                $join->on('appointments.user_id', '=', 'users.id');
            })
            ->get();
    }
    public function createAppointment(array $appointmentDetails){
        return Appointment::create($appointmentDetails);
    }
    public function updateAppointment(array $appointmentDetails, $appointmentId){
        $appointment = Appointment::findOrFail($appointmentId);
        $appointment->update($appointmentDetails);
    }
    public function deleteAppointment($appointmentId){
        return Appointment::where('user_id', auth()->user()->id)
            ->where('id', $appointmentId)
            ->delete();
    }
}
