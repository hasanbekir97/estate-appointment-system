<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Contact;
use Carbon\Carbon;
use Illuminate\Http\Request;


use App\Interfaces\AppointmentRepositoryInterface;
use App\Interfaces\ContactRepositoryInterface;

use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;

class AppointmentController extends Controller
{
    private AppointmentRepositoryInterface $appointmentRepository;
    private ContactRepositoryInterface $contactRepository;

    public function __construct(AppointmentRepositoryInterface $appointmentRepository, ContactRepositoryInterface $contactRepository)
    {
        $this->appointmentRepository = $appointmentRepository;
        $this->contactRepository = $contactRepository;
    }

    public function index(Request $request){
        if(isset($request->date)){
            return response()->json(
                $this->appointmentRepository->getFilteredAppointments($request->date)
                , 200);
        }
        else {
            return response()->json(
                $this->appointmentRepository->getAllAppointments()
                , 200);
        }
    }

    public function show($appointmentId){
        return response()->json(
            $this->appointmentRepository->getAppointment($appointmentId)
            , 200);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:1,255',
            'surname' => 'required|string|between:1,255',
            'email' => 'required|string|email|max:100',
            'phone' => ['required', 'digits_between:0,15', 'numeric'],
            'address' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user_id = auth()->user()->id;

        $destinationInfo = $this->calculateDestination($request->address);

        if($destinationInfo['message'] === false){
            return response()->json([
                'message' => 'Invalid postcode'
            ],404);
        }

        // store data in db
        $appointmentDetails = [
            'user_id' => $user_id,
            'distance' => $destinationInfo['distance'],
            'leave_time' => $destinationInfo['leave_time'],
            'arrival_time' => $destinationInfo['arrival_time']
        ];
        $appointment = $this->appointmentRepository->createAppointment($appointmentDetails);

        $contactDetails = [
            'user_id' => $user_id,
            'appointment_id' => $appointment->id,
            'name' => $request->name,
            'surname' => $request->surname,
            'email' => $request->email,
            'phone' => $request->phone,
            'postcode' => $request->address,
            'explicit_address' => $destinationInfo['destination_address']
        ];
        $this->contactRepository->createContact($contactDetails);

        $userName = User::where('id', $user_id)->get();

        return response()->json([
            'message' => 'Appointment successfully updated',
            'appointment' => [
                'id' => $appointment->id,
                'user_id' => $user_id,
                'user_name' => $userName[0]->name,
                'distance' => $destinationInfo['distance'],
                'leave_time' => $destinationInfo['leave_time'],
                'arrival_time' => $destinationInfo['arrival_time'],
                'customer_name' => $request->name,
                'customer_surname' => $request->surname,
                'customer_email' => $request->email,
                'customer_phone' => $request->phone,
                'customer_postcode' => $request->address,
                'customer_explicit_address' => $destinationInfo['destination_address']
            ]
        ], 201);
    }

    public function update(Request $request, $appointmentId) {
        $validator = Validator::make($request->all(), [
            'name' => 'string|between:1,255',
            'surname' => 'string|between:1,255',
            'email' => 'string|email|max:100',
            'phone' => ['digits_between:0,15', 'numeric']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        if(isset($request->address)) {
            $destinationInfo = $this->calculateUpdateDestination($request->address, $appointmentId);

            if($destinationInfo['message'] === false){
                return response()->json([
                    'message' => 'Invalid postcode'
                ],404);
            }

            // update data in db
            $appointmentDetails = [
                'distance' => $destinationInfo['distance'],
                'leave_time' => $destinationInfo['leave_time'],
                'arrival_time' => $destinationInfo['arrival_time']
            ];
            $this->appointmentRepository->updateAppointment($appointmentDetails, $appointmentId);

            $contactDetails = [
                'name' => $request->name,
                'surname' => $request->surname,
                'email' => $request->email,
                'phone' => $request->phone,
                'postcode' => $request->address,
                'explicit_address' => $destinationInfo['destination_address']
            ];
            $this->contactRepository->updateContact($contactDetails, $appointmentId);
        }
        else {
            $this->contactRepository->updateContact($request->all(), $appointmentId);
        }

        $appointment = $this->appointmentRepository->getAppointment($appointmentId);

        return response()->json([
            'message' => 'Appointment successfully updated',
            'appointment' => $appointment
        ], 201);
    }

    public function delete($appointmentId){
        $this->appointmentRepository->deleteAppointment($appointmentId);
        $this->contactRepository->deleteContact($appointmentId);

        return response()->json(null, 204);
    }

    protected function calculateDestination($destination_address){
        $from = "cm27pj";
        $to = $destination_address;

        // checks that the postcode is valid or not.
        $responseValidatePostalCode = Http::get("api.postcodes.io/postcodes/$to/validate");
        $responsePostalCode = Http::get("api.postcodes.io/postcodes/$to");

        $dataValidatePostalCode = json_decode($responseValidatePostalCode->body());
        $dataPostalCode = json_decode($responsePostalCode->body());

        if($dataValidatePostalCode->result !== true || $dataPostalCode->result->country !== "England"){
            return [
                'message' => false,
            ];
        }


        // calculate distance between two points
        $response = Http::get('https://maps.googleapis.com/maps/api/distancematrix/json', [
            'origins' => $from,
            'destinations' => $to,
            'key' => 'AIzaSyAnBGsp4eZ7MQ8Yk60k_bd-4C1c459KKl0'
        ]);

        $data = json_decode($response->body());

        $distance = $data->rows[0]->elements[0]->distance->text;
        $duration = ($data->rows[0]->elements[0]->duration->value)/60;


        $available_times = Appointment::select('leave_time', 'arrival_time')
            ->orderBy('arrival_time', 'ASC')
            ->get();

        $leave_time = Carbon::now();
        $arrival_time = Carbon::parse($leave_time)->addMinutes(($duration * 2) + 60);

        for($i=0; $i<count($available_times); $i++){

            $leave_time = Carbon::parse($available_times[$i]->arrival_time);
            $arrival_time = Carbon::parse($available_times[$i]->arrival_time)->addMinutes(($duration * 2) + 60);


            if($leave_time < Carbon::now())
                continue;

            if($i === (count($available_times) - 1))
                continue;

            // if there is available time between appointments, then it will gets this arrival time from table as leave time.
            if($arrival_time < $available_times[$i+1]->leave_time){
                break;
            }
        }

        return [
            'message' => true,
            'distance' => $distance,
            'destination_address' => $data->destination_addresses[0],
            'leave_time' => $leave_time,
            'arrival_time' => $arrival_time
        ];
    }

    protected function calculateUpdateDestination($destination_address, $appointmentId){
        $from = "cm27pj";
        $to = $destination_address;

        // checks that the postcode is valid or not.
        $responseValidatePostalCode = Http::get("api.postcodes.io/postcodes/$to/validate");
        $responsePostalCode = Http::get("api.postcodes.io/postcodes/$to");

        $dataValidatePostalCode = json_decode($responseValidatePostalCode->body());
        $dataPostalCode = json_decode($responsePostalCode->body());

        if($dataValidatePostalCode->result !== true || $dataPostalCode->result->country !== "England"){
            return [
                'message' => false,
            ];
        }


        // calculate distance between two points
        $response = Http::get('https://maps.googleapis.com/maps/api/distancematrix/json', [
            'origins' => $from,
            'destinations' => $to,
            'key' => 'AIzaSyAnBGsp4eZ7MQ8Yk60k_bd-4C1c459KKl0'
        ]);

        $data = json_decode($response->body());

        $distance = $data->rows[0]->elements[0]->distance->text;
        $duration = ($data->rows[0]->elements[0]->duration->value)/60;


        $available_times = Appointment::select('leave_time', 'arrival_time')
            ->where('id', '!=', $appointmentId)
            ->orderBy('arrival_time', 'ASC')
            ->get();

        $leave_time = Carbon::now();
        $arrival_time = Carbon::parse($leave_time)->addMinutes(($duration * 2) + 60);

        for($i=0; $i<count($available_times); $i++){

            $leave_time = Carbon::parse($available_times[$i]->arrival_time);
            $arrival_time = Carbon::parse($available_times[$i]->arrival_time)->addMinutes(($duration * 2) + 60);


            if($leave_time < Carbon::now())
                continue;

            if($i === (count($available_times) - 1))
                continue;

            // if there is available time between appointments, then it will gets this arrival time from table as leave time.
            if($arrival_time < $available_times[$i+1]->leave_time){
                break;
            }
        }

        return [
            'message' => true,
            'distance' => $distance,
            'destination_address' => $data->destination_addresses[0],
            'leave_time' => $leave_time,
            'arrival_time' => $arrival_time
        ];
    }
}
